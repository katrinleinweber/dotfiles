#!/usr/bin/env bash

# Requires https://github.com/tummychow/git-absorb#git-absorb

#files=("$@")
# From https://stackoverflow.com/a/39780904/4341322
#for ((i = 0; i < ${#files[@]}; ++i)); do
#    (IFS=' '; git add "${files[i]}")
#
#    git absorb --and-rebase
#done

git add $@
git absorb --force --and-rebase
