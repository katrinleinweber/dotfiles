#!/usr/bin/env bash

term="$1"

maybe_branch_name=$(
  git branch --sort=-committerdate |
    rg "$term" |
    head -n 1
)

if [ -n "$maybe_branch_name" ]; then
  git switch "${maybe_branch_name:2}"
else
  echo "Could not find a branch name that matches: $term"
fi
