#!/usr/bin/env bash

# Use with aliases vw (low qual) & vww (high) to avoid ads on YouTube.

format="$1"
url="$2"

id=$(echo "$url" | rg --only-matching --pcre2 '(?<=v=|\/)(-|\w){11}')

function dl_and_watch() {
  yt-dlp --no-playlist --restrict-filenames --format "$format" -- "$id" &&
    vlc --fullscreen --rate=1.33 -- ./*"$id"*
}

if ! dl_and_watch; then
  yt-dlp --list-formats "$url"

  echo -e "\nyt-dlp --no-playlist --restrict-filenames --format= \ \n$url \ "
  echo -e "&& vlc --fullscreen --rate=1.33 -- ./*$id*"
fi

echo "rip -- ./*\"$id\"*" | pbcopy

# - [ ] move alias into private section
