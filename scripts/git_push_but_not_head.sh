#!/usr/bin/env zsh

branch=${1:-main}
N_commits=${2:-1}

git push --force origin HEAD~"$N_commits":"$branch"
