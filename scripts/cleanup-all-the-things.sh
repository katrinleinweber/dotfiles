#!/usr/bin/env bash

# Remove Homebrew cache older than 1 week,
# "including downloads for even the latest versions",
# but not installed downloads

chronic parallel -- \
  "brew cleanup --prune=7 --quiet -s" \
  "docker system prune --force --all --filter='until=240h'" \
  "docker system prune --force --volumes"

# Also remove old, installed downloads
# https://github.com/Homebrew/brew/issues/3784#issuecomment-364675767
find "$(brew --cache)"/downloads \
  -name '*--*.{json,tar.gz}' \
  -type f \
  -atime +7 \
  -print0 | xargs -0 rip {}

# Also remove build caches in my own folders.
kondo \
  --all --quiet --quiet -- \
  ~/*.com \
  ~/*.io \
  ~/*.org \
  ~/Downloads \
  ~/src \
  ~/Library/Caches

rip ~/builds || true

cd ~/GitLab.com/GDK && chronic parallel -- \
  "gdk cleanup" \ 
  "yarn cache clean"

time_machine_snapshots=$(tmutil listlocalsnapshotdates | grep "-")
echo "All Time Machine snapshots: $time_machine_snapshots"
echo "Deleting Time Machine snapshots: $(echo $time_machine_snapshots | sort | awk 'NR==1; END{print}')"
# for snap in $time_machine_snapshots; do
#   sudo tmutil deletelocalsnapshots "$snap"
# done
