#!/bin/bash
# shellcheck disable=SC2185,SC2086

# Garbage-collect all repos under current working dir.
# Up to the given depth.

fnd=$(which find || /usr/bin/find)
git=$(which git  || /usr/bin/git)

depth=${1:-2} # Some limit here seems better than none at all.
repos=$("$fnd" . -maxdepth "$depth" -type d -name '.git')

function du_without_path() {
  du -sh "$1" | sed -E 's;\./.+$;;'
}

for r in $repos; do
  repo=$(dirname "$r")
  size=$(du_without_path "$repo")

      >&2 echo -e "\n$repo"

  remote=$("$git" -C "$repo" remote show)
  "$git" -C "$repo" remote set-head "$remote" --auto 1>/dev/null

  "$git" -C "$repo" trim --no-update --no-confirm 1>/dev/null
  "$git" -C "$repo" gc --aggressive --keep-largest-pack --quiet

        >&2 echo -e "  before: $size\n   after: $(du_without_path $repo)"
done
