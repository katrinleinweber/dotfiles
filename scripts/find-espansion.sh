#!/usr/bin/env bash

term="$1"

# Find an Espanso trigger or replacement, and highlight the trigger

espanso match list |
  rg "$term"
