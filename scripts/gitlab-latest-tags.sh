#!/usr/bin/env bash

# git -C ~/src/gitlab.com/GDK/gitlab fetch origin --tags
git -C ~/src/gitlab.com/GDK/gitlab tag --list |
    rg --only-matching '\d{2}(\.\d+){2}' |
    sort --version-sort --unique |
    tail -10 | column
