#!/usr/bin/env bash

base_path=~/Sync/K
st_glob="*-conflict*"

data_file="$base_path/CO2Signal.tsv"

# First, show today's electricity mix
open "https://energy-charts.info/charts/consumption_advice/chart.htm?l=de&c=DE"

# Secondly, download free data from ElectricityMap (partner?)
# echo "datetime	countryCode	carbonIntensity	units	fossilFuelPercentage" >> $data_file
# Using tabs here to improve stdout-put later.
# Otherwise, `xsv table $data_file` would be needed.

function get_tsv_row() {
  curl --silent --header "auth-token: $CO2SIGNAL_PAT" \
    "https://api.co2signal.com/v1/latest?countryCode=$1" |
    jq --raw-output \
      '[
    .data.datetime,
    .countryCode,
    .data.carbonIntensity,
    .units.carbonIntensity,
    .data.fossilFuelPercentage
  ] | @tsv' |
    # truncating is easier than rounding
    sd '\.\d+$' ''
}

for country in \
  "DE" \
  "ES"; do
  get_tsv_row "$country" >>$data_file
done

# [ ] fix cat: /Users/katrinleinweber/Sync/K/.: Is a directory
# cat $base_path/$data_name.* | sort -r | uniq | sponge "$data_file"

egrep --max-count=4 --only-matching '(fossil|DE|ES|\d+$)' $data_file

trash $base_path/$plot_name$st_glob &>/dev/null

# further ideas
# [x] add countryCode column
# [x] round fossilFuelPercentage
#     - neither `round(.data.fossilFuelPercentage)` nor `.data.fossilFuelPercentage | round` works
# [ ] draw termplot or similar
# [x] remove duplicate entries with `sponge | sort -r | uniq`
# [ ] add to cron
