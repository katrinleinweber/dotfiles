#!/bin/bash

url=$1
dep=${2:-2}

# Extract directory name from URL: anything after last slash and without ".git"
dir=$(basename "$url" .git)
# dir=$(echo "$url" | rg --only-matching --pcre2 '(?<=/)[^/]+/?$' | cut -d'.' -f1)
# NOT NEEDED if we simply accept to copy-paste the git-grab output

cmd="git grab $url -- \
--depth=$dep \
--origin=upstream \
--shallow-submodules \
--recurse-submodules \
--also-filter-submodules \
--filter=tree:0 \
"

# Inject blob filter unless the hoster is known to not support combined filters.
if ! echo "$url" | grep 'github'; then
  cmd=${cmd//filter=/filter=blob:none --filter=}
fi

eval "$cmd"

echo "git config --list | grep 'user.'"
echo "git remote add origin git@:/$dir.git"
echo "# !!! Construct the fork ^ URL ^ above and then execute with  cd  in front of the cloned folder."
