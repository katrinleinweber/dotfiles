#!/usr/bin/env bash

for f in $*; do
  exiftool -All= "$f"
done

sleep 1
rip *_original

# https://stackoverflow.com/questions/5694823/
shrink=33
cat <<MSG
\n💡 To also shrink images:

$(which mogrify) -white-balance -resize ${shrink}% $*
MSG
