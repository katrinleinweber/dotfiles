#!/bin/bash

for file in $*; do
  sd ':16\d{8}:' ':16…time…:' "$file"
done

codium --diff $*
