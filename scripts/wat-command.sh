#!/usr/bin/env bash

# Figure out where some alias, function, asdf shim, or normal command,
# actually points to on-disk, and show that path and its contents.

# Resolve search term the POSIX way. Avoid `which`!
resolved=$(command -v "$1")

# If it's an alias, it's not loaded in non-interactive shell,
# so we'll search for it on-disk.
if [[ $? -ne 0 ]]; then
  resolved=$(
    rg --no-filename --no-line-number "$1" \
      "$HOME"/.config/aliases \
      "$HOME"/.config/functions |
      sd '^alias.+=' '' |
      sd "'" ""
  )
fi

# File targets should at this point have only the path
/bin/cat $resolved 2>/dev/null

# Functions will at this point have not been resolved
if [[ $? -ne 0 ]]; then
  which "$1"
else
  # Try printing the version
  eval "$resolved version" ||
    eval "$resolved --version" ||
    eval "$resolved -v" ||
    eval "man $resolved | head -1" ||
    eval "$resolved --help | head -1" ||
    eval "$resolved -h | head -1" || true
fi
