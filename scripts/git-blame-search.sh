#!/usr/bin/env bash

term="$1"
tmp="/tmp/git-blame-search-$(date -u +%y%m%d-%H%M%S).txt"
echo -e "# Search term: $term" >"$tmp"

# Find all matches in files
rg --glob='!locale' --hidden --files-with-matches "$term" | while read file; do

  # Log details
  {
    echo -e "\n\n\n### git blame $file | rg $term\n"
    git blame -w -s --show-name --show-email -- "$file" |
      rg "$term" |
      sed "s,\x1B\[[0-9;]*[a-zA-Z],,g;s,\x0D\x0A,\x0A,g"
  } >>"$tmp"
  # Append to $file? For moving to the relevant code location.
  # Would be rebuilding a "blame popup", though!
done

# Show results
rg --only-matching '^[0-9a-f]+\s' "$tmp" | sort | uniq -c | sort
eval "$EDITOR $tmp"
# Also loop through these & do git show $id | rg $term
# echo -e "\n\n\n### git show $id | rg $term\n";
