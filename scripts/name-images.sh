#!/bin/bash

# Naming default DCIM images according to: YYMMDD[a-z]-Event-slug.ext
#
# USAGE EXAMPLE
#
#   ./name-image.sh Event 20120102*
#   # 2012010203_123456.png 2012010203_135700.png
#   ls
#   12010203a-Event.png   12010203b-Event.png

# Extract 1st argument and use it as slug for event, location, etc.
# https://unix.stackexchange.com/a/367256
slug="$1"
shift

# Guard against non-unique IDs
# TODO: Correctly count filenames with space in them as 1 file.
files=("$@")
n=$(echo "$@" | tr ' '  "\n" | wc -l)
z=26
if [[ $n -ge $z ]]; then
  echo -e "\nTOO MANY input files: ${#files[*]}!\n\n$(echo "$@" | tr ' '  '\n')\n"
  echo "But only $z ASCII letters are available to keep IDs unique."
  echo "Please supply a narrower glob pattern!"
  exit "$z"
fi

# Trim useless prefixes and _hhmmss off the filename
# Screenshots use …DD-hh… and _App name
# TODO: Keep hhmmss iff n>26
function trim_name()  {
  basename "$1" "$2" | sd '^(IMG|Screenshot)?_?20' '' | sd '(_|-)\d+(_.+)?\.$' ''
}

# Extract file extension
# https://stackoverflow.com/a/43246376
function file_type() {
  echo "$1" | cut -d '.' -f 2
}

# Iterate over remaining arguments, suffixing YYMMDD-ID with letters
# https://stackoverflow.com/a/29288523
chars=({a..z})
for ((i = 0; i < n; i++)); do
  char=${chars[i]}
  file=${files[i]}
  type=$(file_type "${file}")
  name=$(trim_name "${file}" "$type")
  id="${name}${char}"

  # Skip characters that have been assigned in previous YYMMDD… IDs
  # https://stackoverflow.com/a/42714096
  existing=$(/bin/ls | grep --count "$id" || true)
  if [[ $existing -gt 0 ]]; then
    declare -a 'chars=($( echo {'"$char"'..z}))'
    echo "SKIP existing $char and set chars to $chars..."
    continue
  fi

  new_name="$id-${slug}.${type}"
  mv "$file" "$new_name"
done
