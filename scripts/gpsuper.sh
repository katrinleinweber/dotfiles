#!/usr/bin/env bash

user="$1"
title="$2"

remote=${3:-origin}
release=${4:-Next 1-3 releases}

# From https://github.com/ohmyzsh/ohmyzsh/blob/d87ab25/lib/git.zsh#L6-L10
function __git_prompt_git() {
  GIT_OPTIONAL_LOCKS=0 command git "$@"
}

# From https://github.com/ohmyzsh/ohmyzsh/blob/d87ab25/lib/git.zsh#L92-L105
function git_current_branch() {
  local ref
  ref=$(__git_prompt_git symbolic-ref --quiet HEAD 2>/dev/null)
  local ret=$?
  if [[ $ret != 0 ]]; then
    [[ $ret == 128 ]] && return  # no git repo.
    ref=$(__git_prompt_git rev-parse --short HEAD 2>/dev/null)  || return
  fi
  echo "${ref#refs/heads/}"
}

bundle install --quiet || true
git log --format='%an %ae' | head -12 | sort | uniq -c | grep -vE '(^\s+1 |GitLab-Bot|_bot_)'

if git remote get-url "$remote" | grep gitlab.com >/dev/null; then
  git push --set-upstream "$remote" "$(git_current_branch)" \
    -o merge_request.milestone="$release" \
    -o merge_request.assign="$user" \
    -o merge_request.title="$title" \
    -o merge_request.create \
    -o ci.skip
    # More -o ptions:
    #   1. https://docs.gitlab.com/ee/topics/git/commit.html#push-options-for-merge-requests
    #   2. https://git-scm.com/docs/git-push#Documentation/git-push.txt-pushpushOption
else
  git push --set-upstream "$remote" "$(git_current_branch)"
fi
