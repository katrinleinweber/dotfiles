#!/bin/bash

hist="$HOME/.zsh_history"
dump=/tmp/shell_hist
chez="$HOME/.local/share/chezmoi/dot_config/aliases.tmpl"

# Get clean history of commands
rg --only-matching --pcre2 '(?<=;).+$' "$hist" | uniq -c >"$dump"

aliases=$(
  rg --only-matching --pcre2 '(?<=alias |-g )\w+(?==)' "$chez"
)

for alias in $aliases; do
  n=$(ug --count --smart-case --word-regexp "^\s+\d+ $alias" "$dump")
  echo "$n x $alias"
done |
  sort --numeric |
  ug '^\d ' |
  column
