#!/bin/bash

# This script helps with data extraction from GitLabSOS & kubeSOS archives

### REQUIREMENTS

# 1. https://gitlab.com/gitlab-com/support/toolbox/gitlabrb_sanitizer cloned to this path:

sanitize_gitlab_rb() {
  for GLRB in $(rg --files-with-matches '^\b*external_url\b+'); do
    ruby ~/src/gitlab.com/support-toolbox/rb-sanitizer/sanitizer \
      --file="$GLRB" >_"$GLRB"
    mv _"$GLRB" "$GLRB"
  done
}

# 2. These tools installed & in $PATH:
#    https://gitlab.com/gitlab-com/support/toolbox/fast-stats
#    https://gitlab.com/gitlab-com/support/toolbox/greenhat
#    https://github.com/BurntSushi/ripgrep
#
# 3. Finally, a GitLabSOS an/or kubeSOS archive in a ~/Downloads/zd-… subfolder.

### MORE HELPER FUNCTIONS

find_k8SOSes() {
  compgen -G "kubesos*.t*gz"
}

trim_timestamp() {
  rg --only-matching --pcre2 '(?<= )\{.+\}'
}

for k8SOS in $(find_k8SOSes); do

  # Prepare folder & file names
  TMP=$(echo "$k8SOS" | sed -E 's/\.t(ar\.)?gz$//')
  DIR=$(echo "$TMP" | sed -E 's/kubesos-//')
  PRE=gl-"$1"-fast-stats

  if [[ -e "$DIR" ]]; then
    continue
  else
    tar xf "$k8SOS" || continue
    #mv "$k8SOS" extracted-"$k8SOS" || continue
    mv "$TMP" "$DIR"  || continue
    cd "$DIR" || continue

    for f in \
      webservice_webservice \
      sidekiq_sidekiq \
      gitlab-shell_gitlab-shell \
      gitaly_gitaly; do
        name=$(echo "$f" | cut -d'_' -f1)
        file="$PRE-$name.txt"
        json=$(/bin/cat "$f"* | trim_timestamp)

        { # Gather statistics separately, but remove lines about the least impactful items
          # awk 'NR==1; END{print}' "$json" | jq --raw-output '.time' | sd '\.\d+Z' ''

          printf "\n# %s\n\n" "$name"

          echo "$json" | fast-stats errors
          printf "\n\n"

          echo "$json" | fast-stats top --sort-by=duration --display=percentage |
            sd '^(\s+[0-3])+ -- .+\n' ''
          printf "\n\n"

          echo "$json" | fast-stats --verbose |
            grep -vE '^\s+(MAX|(P\d+\s+([a-z]+: [0-3]s\s+)+)-- )'
      }   >"$file"

      # echo -e "\nTop load causers"
      # rg --before-context=2 --after-context=10 'FAIL_CT -- (CLIENT|PATH|PROJECT|USER)'

      # PLOT="$PRE-$f".png
      # fast-stats plot --outfile "$PLOT" "$f"
    done

    # Delete empty byte files & open others
    # Default fast-stats scaffold if ca. 150 bytes
    find . -type f -size -160c -iname "$PRE*"
    open "$PRE-*.png"
    eval "$EDITOR . $PRE-*.txt"

    cd ..
  fi
done

find_glSOSes() {
  compgen -G "gitlabsos.*.t*"
}

greenhat_report() {
  greenhat --no-prompt --report=full "$1" |
    sed "s,\x1B\[[0-9;]*[a-zA-Z],,g;s,\x0D\x0A,\x0A,g" |
    tee -a "$2"
}

check_versions() {
  cat ./**/version-manifest.txt | sort | uniq -c | sort -nr | tail
  echo -n "If one of these ☝️  lines diverges, "
  echo    "and does not start with the number of SOSes, "
  echo -n "it's either due to upgrades between the SOSes, "
  echo    "or a version mismatch between node."
}

# See https://about.gitlab.com/support/#please-dont-send-files-in-formats-that-can-contain-executable-code
convert_pdfs() {
  for pdf in *.pdf; do
    pdftoppm -png "$pdf" "$pdf".png
    mv "$pdf" AVOID_"$pdf"
  done
}

wipe_outdated_downloads() {
  ruby ~/src/gitlab.com/support-toolbox/zd-dl-wiper/zd-dl-wiper.rb --days-to-keep=40 # Remove later in Jan. 2025
}

### MAIN PROCEDURE :blush:

if compgen -G "*.pdf" >/dev/null; then
  convert_pdfs
fi

sanitize_gitlab_rb

# https://gitlab.com/gitlab-com/support/toolbox/gitlabsos#run-the-script
# https://gitlab.com/greg/scripts/-/blob/master/sos-alyzer.sh
for SOS in $(find_glSOSes); do
  # Prepare folder names
  echo -e "\n\nProcessing SOS file: $SOS\n"

  DIR=$(tar tvf "$SOS" | grep '^d' | head -1 | awk '{ print $NF }' | sed -e 's;/$;;')

  if [[ -e "$DIR" ]]; then
    sleep 0.5
    continue
  else
    # Unpack only new SOS archives
    tar xf "$SOS"
    XYZ=${DIR//gitlabsos./}
    FIN=${XYZ//_gitaly-nginx*psql-puma-redis-sidekiq/}
    host=$(echo "$FIN" | sed -E 's/_[0-9]+//')

    # Remove common host or suffix
    if [ -z "$2" ]; then
      host=$(echo "$host" | sed -e "s/^$2//" | sed -e "s/$2\$//")
    fi

    sleep 0.5
    mv "$DIR" "$FIN"

    # Create "sister" folder for analysis output
    FS_DIR="$FIN"-FS
    GREENHAT_FILE="$FS_DIR"/gl-"$1"-greenhat-report-"$host".txt
    ERROR_FILE=../"$FS_DIR"/gl-"$1"-fast-stats-errors-"$host".txt
    mkdir "$FS_DIR" || true

    # greenhat_report "$SOS" "$GREENHAT_FILE"

    cd "$FIN"       || true

    LOG_PATH=var/log/gitlab
    GITALY="$LOG_PATH"/gitaly/current
    GITATS="$LOG_PATH"/gitaly/@*.s
    PRODUCTION="$LOG_PATH"/gitlab-rails/production_json.log

    # Remove red herrings & false-positive "errors"
    if [ -f $GITALY ]; then
      mv "$GITALY" "$GITALY".ori
      rg --invert-match '(not a git repository: .*\.(design|wiki)\.git|error","msg":"PID \d+ BUNDLE_GEMFILE)' \
        "$GITALY".ori \
        >"$GITALY"
    fi

    if [ -f "$GITATS" ]; then
      ( 
        ( cat "$GITATS" | gunzip )
        at "$GITALY"
      ) >"$GITALY".full
      mv "$GITALY".full "$GITALY"
    fi

    if [ -f $PRODUCTION ]; then
      rm "$LOG_PATH"/gitlab-rails/production.log

      mv "$PRODUCTION" "$PRODUCTION".ori
      rg --invert-match --fixed-strings 'Cannot obtain an exclusive lease for ci/pipeline_processing/atomic_processing_service::pipeline_id' \
        "$PRODUCTION".ori \
        >"$PRODUCTION"
    fi

    rip "$LOG_PATH"/**/*.ori || true

    { # Check for Puma OOMs
      printf "### PumaWorkerKiller: Out of memory\n"
      ug --fixed-strings 'PumaWorkerKiller: Out of memory' "$LOG_PATH"/puma/puma_stdout.log |
        jq --raw-output '.timestamp' |
        cut -d':' -f1-2 | sort | uniq -c
      printf "\n\n"

      printf "### PumaHandler: RSS memory limit exceeded\n"
      ug --fixed-strings 'message":"rss memory limit exceeded' \
        "$LOG_PATH"/gitlab-rails/application_json.log |
        jq --raw-output '.time' |
        cut -d':' -f1-2 | sort | uniq -c
      printf "\n\n"

    } >>"$ERROR_FILE"

    # Summarise fast-stats-compatible files
    for LOG in \
      gitlab-rails/api_json.log \
      gitlab-rails/production_json.log \
      praefect/current \
      gitaly/current \
      sidekiq/current; do
      SUM="$LOG_PATH"/$LOG

      if [ ! -e "$SUM" ]; then continue; fi

      SOURCE=$(echo "$LOG" | sed -E 's;(gitlab-(rails|shell)/|_json.log|/current);;g')
      # PLOT=../"$FS_DIR"/gl-"$1"-fast-stats-"$SOURCE"-"$host".png
      # fast-stats plot --outfile "$PLOT" "$SUM"

      {
        printf "\n# %s\n\n" "$LOG"
        fast-stats errors "$SUM" |
          grep -vE '^│(Backtrace|Count|Events):' |
          grep -vE '(├─+[┬┴]─+[┘┐]|│ +│)'
        printf "\n\n"
      } >>"$ERROR_FILE"

      file=$(echo "$LOG" | sed 's;_json.log;;' | sed -E 's;(gitlab-(rails|shell)/|/current);;')
      STATS_FILE=../"$FS_DIR"/gl-"$1"-fast-stats-"$host"-"$file".txt

      { # Gather statistics separately, but remove lines about the least impactful items
        awk 'NR==1; END{print}' $SUM | jq --raw-output '.time' | sd '\.\d+Z' ''

        printf "\n# %s\n\n" "$LOG"
        fast-stats top --sort-by=duration --display=percentage "$SUM" |
          sd '^(\s+\d)+ -- .+\n' ''
        printf "\n\n"

        printf "\n\n"
        fast-stats --verbose "$SUM" |
          sd '^\s+(MAX|P\d+)\s+([a-z]+: [0-2]s\s+)+-- .+\n' ''
      } >>"$STATS_FILE"

      # or --display=percentage --sort-by=duration gitaly/current | sd '^[a-zA-Z0-9_\-./]+\s+(\s+[01])+$\n' ''
      # https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/path_regex.rb#L132-133

      sleep 0.5
    done

    { # Summarise exceptions
      printf "\n\n\n### Exceptions\n\n"
      jq --raw-output 'select(.severity == "ERROR") | [.time, ."meta.caller_id", ."exception.class", ."exception.message"] | @tsv' "$LOG_PATH"/gitlab-rails/exceptions_json.log |
        sd '\d:\d{2}\.\d{3}Z' '' |
        sort | uniq -c | sort
      printf "\n\n"
    } >>"$ERROR_FILE"

    { # Gather other notable log entries
      printf "\n\n\n### Remaining problems\n\n"
      rg '" (4[0-9]|5[0-29])[0-9] \d+ "' "$LOG_PATH"
      rg --glob='!reconfigure' --ignore-case '\b(backtrace|C(an|ould) not|eof|err(or)?|denied|fail(ed)?|fata(l)?|invalid|kill(ed)?|panic|Rack_Attack|rake aborted|time.?out|warn(ing)?)\b' "$LOG_PATH"
    } | ug --invert-match 'keywatcher: pubsub receive: EOF' \
      >>"$ERROR_FILE"
    # Ignore Workhorse/Redis log noise: https://gitlab.com/gitlab-org/gitlab/-/issues/426006

    cd ..

    sleep 0.5
  fi
done

if compgen -G "./*-FS/*.txt"  >/dev/null; then
  sd '\.\d{2}\b' '   ' ./*/sar_*

  # Output quick DB & manifest overview
  ug 'down' ./**/gitlab_migrations
  check_versions

  # Aggregate usage patterns
  find . -type f -name 'api_json.log' -exec cat '{}' '+' |
    jq --raw-output '[.username, .ua, .route] | @tsv' |
    sort | uniq -c | sort -rn \
    >user_agents.txt

  find . -type f -path '*/gitaly/current' -exec cat '{}' '+' |
    jq --raw-output '[.username, ."grpc.method"] | @tsv' |
    sort | uniq -c | sort -rn \
    >user_methods.txt

  # Aggregate all Postgres errors
  rg --no-filename 'FATAL:' */var/log/gitlab/postgresql/current |
      sd '(20[2-9]\d-0?|\d:\d+\.\d+)' '…' | sort | uniq -c \
    >pg_errors.txt

  codium ./*-FS/*.txt ./user_*.txt .
fi

wipe_outdated_downloads &
