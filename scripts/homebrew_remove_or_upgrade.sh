#!/bin/bash

# Sometimes, I install formulae just for testing something.
# Upon availability of an upgrade, I should decide more permanently:
# remove or keep them?
#
# The former is the default, hence we ignore stderr there,
# because that indicates another formula depending on it.

set -eux -o pipefail

base="$(chezmoi source-path)"

for i in "$@"; do
  if grep --quiet --word-regexp "$i" "$base"/run_*; then
    continue
  else
    brew remove  "$i" 2>/dev/null ||
    brew upgrade "$i" 1>/dev/null
  fi
done
