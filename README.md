# katrinleinweber's configuration files

Managed with [`chezmoi`](https://github.com/twpayne/chezmoi).

## Bootstrap a new system with

1. [Install Homebrew](https://brew.sh)
1. [Install chezmoi](https://www.chezmoi.io/docs/install/)
1. Review `dot_` and `run_` files in this repo
1. `chezmoi init --verbose --apply https://gitlab.com/katrinleinweber/dotfiles.git`
1. Enter appropriate data as prompted.

## Working with this repo's `Makefile`

- After `make ignores`: Discard deletions of self-configured rules. Appending the gitignore.io rules would have required deduplication.
