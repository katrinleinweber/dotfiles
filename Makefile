ignores:
	echo ".bundle/\n.gitlab-ci-local/\n.idea/\n.stversions/\n*.bak\ncoverage" > dot_config/git/ignore
	curl -sL https://www.toptal.com/developers/gitignore/api/git,macos,rubymine,visualstudiocode >> dot_config/git/ignore

### Cheatsheet
# from http://slides.jcoglan.com/make-scotjs
# and https://www.youtube.com/watch?v=usK89eTnPyc
# and https://github.com/search?q=owner%3Ajcoglan+path%3AMakefile+language%3AMakefile&type=code&l=Makefile
# and https://swcarpentry.github.io/make-novice/key-points.html

# SHELL := /bin/bash
# PATH  := path/to/bin:$(PATH)
## to set a few defaults

# .PHONY: non-file targets = pure scripts

# file_list     := $(wildcard src/glob-*.ext)
# pattern_match := $(file_list:%.ext=%.spec)

# build/%.ext: %.src
#		compiler --out $(dir $@) $<
## output all compiled versions of something/*.scr
## to matching targets in build/something/*.ext

# $@ means current target; before :
# $< means first dependency
# $^ means all dependencies

# $(shell …cmd…) to collect output from Shell command
