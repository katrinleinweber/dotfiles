#!/bin/bash

file=/tmp/cron

/bin/cat >"$file" <<HEREDOC
12 */3 * * * /usr/local/bin/chronic /Users/katrinleinweber/.local/share/chezmoi/scripts/homebrew_upgrade_all
35 12 */8 * * /usr/local/bin/chronic /usr/local/bin/brew cleanup -s
23 11 13 * *  /usr/local/bin/chronic /usr/local/bin/fd --max-depth=1 --glob '*.ldb' ~/Library/Caches/Sublime*/Index --exec /usr/local/bin/rip
HEREDOC

# zd-dl-wiper removes ZenDesk attachment downloads after 20 days and requires:
# 1. https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router
#    installed & configured to create ~/Downloads/zd-… subfolders attachments.
# 2. https://gitlab.com/gitlab-com/support/toolbox/zd-dl-wiper
#    cloned to above-called path.

/usr/bin/crontab "$file"
/usr/bin/crontab -l
